package univpm.advprog.bookshop;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;
@NamedQueries(
		@NamedQuery(
				name="findBooks",
				query = "SELECT b FROM Book b WHERE b.title = :title"
		)
	)
@Entity
@Table(name = "book")
public class Book {
	
	protected long id;
	
	protected String title;
	
	protected Set<Author> authors = new HashSet<Author>();
	
	protected String isbn;
	protected float price;
	
    public Book() {}
	
    @Id
    @Column(name = "book_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long getId() {
		return id;
	}
	
    public void setId(long id) {
    	this.id = id;	
    }
	
    @Column( unique = true )
    public String getTitle() {
    	return title;
    }
    
    public void setTitle(String title) {
    	this.title = title;
    }
    
//	@ManyToOne(fetch=FetchType.LAZY, targetEntity = Author.class)
    @ManyToMany(cascade=CascadeType.PERSIST) 
    @JoinTable(name="AUTHOR_BOOK",
    		joinColumns=@JoinColumn(name="BOOK_ID"),
    		inverseJoinColumns=@JoinColumn(name="AUTHOR_ID")
    )
    public Set<Author> getAuthors() {
    	return authors;
	}

    public void setAuthors(Set<Author> authors) {
		this.authors = authors;
    }

	public void addAuthor(Author a) {
		this.authors.add(a);
	}

    
    @Column
    public float getPrice() {

    	return price;
	}
    
    public void setPrice(float price) {
    	this.price = price;
    }

    @Column
	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}


}