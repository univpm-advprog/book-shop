package univpm.advprog.bookshop;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class BookManager  {
    // Create an EntityManager
    private static EntityManagerFactory ENTITY_MANAGER_FACTORY; // = Persistence
            //.createEntityManagerFactory("puBookStore");
    
    AuthorManager authorManager;
    EntityManager manager;
//    EntityTransaction transaction = null;

	protected void setup() {
		if (ENTITY_MANAGER_FACTORY == null) {
			ENTITY_MANAGER_FACTORY = Persistence
					.createEntityManagerFactory("puBookStore");
		}
		this.manager = ENTITY_MANAGER_FACTORY.createEntityManager();
	}

	public BookManager() {
		setup();		
	}
	
	public BookManager(EntityManager manager) {
		if (manager == null) {
			this.setup();
		} else {
			this.manager = manager;
		}
	}
	
	public void setAuthorManager(AuthorManager authorManager) {
		this.authorManager = authorManager;
	}
	
//	@Override
//	public void close() {
//		this.manager.close();
//	}

	
	public Book create(String title, float price, String authorFirstName, String authorLastName) {
		
		Book book = new Book();
		book.setTitle(title);
		
		//AuthorManager am = new AuthorManager();
		Author a = this.authorManager.getOrCreate(authorFirstName, authorLastName);
		book.addAuthor(a);
		
		book.setPrice(price);

		try {
//			transaction = manager.getTransaction();
//			
//			transaction.begin();
			
			manager.persist(book);
			
//			transaction.commit();
			
			
		} catch (Exception ex) {
            // If there are any exceptions, roll back the changes
//            if (transaction != null) {
//                transaction.rollback();
//            }
            // Print the Exception
            ex.printStackTrace();
		}
		
		return book;
	}

	public Book read(long bookId) {
		Book book = null;
		
		try {
//			transaction = manager.getTransaction();
//			
//			transaction.begin();
			CriteriaBuilder cb = this.manager.getCriteriaBuilder();
			
			CriteriaQuery criteria = cb.createQuery();
			Root e = criteria.from(Book.class);
			criteria.select(e).where(cb.equal(e.get("id"), bookId));
			Query query = this.manager.createQuery(criteria);
			
			book = (Book)query.getSingleResult();
			//Query query = this.manager.createQuery(criteria.select().where(cb.equal(x, y)));
			
			//List<Book> books = (List<Book>) manager.createQuery("SELECT a FROM Book a WHERE id = " + bookId).getResultList();
			/*
			if (books.size() == 0) {
				throw new Exception("Author with ID " + bookId + " not present");
				
			} else if (books.size() > 1) {
				
				throw new Exception("Too many authors with ID " + bookId + ": " + books.size());
			}
			
			
			book = books.get(0);
			*/
			System.out.println("Title: " + book.getTitle());
			System.out.println("Price: " + book.getPrice());
			

//			transaction.commit();
			

		} catch (Exception ex) {
            // If there are any exceptions, roll back the changes
//            if (transaction != null) {
//                transaction.rollback();
//            }
            // Print the Exception
            ex.printStackTrace();
			
		}

		return book;
	}

	public void update(long bookId, String new_title, float new_price, String authorFirstName, String authorLastName) {
		Book book = new Book();
		book.setId(bookId);
		book.setTitle(new_title);
		book.setPrice(new_price);
		
		this.update(book, authorFirstName, authorLastName);
	}
	
	public Book update(Book book, String authorFirstName, String authorLastName) {

		try  {
			Author author = this.authorManager.getOrCreate(authorFirstName, authorLastName);
			book.addAuthor(author);
		} catch (Exception ex) {
			throw ex;
		}

		
		try {
//			transaction = manager.getTransaction();
//			
//			transaction.begin();

			book = manager.merge(book);
//			transaction.commit();
			
		} catch (Exception ex) {
            // If there are any exceptions, roll back the changes
//            if (transaction != null) {
//                transaction.rollback();
//            }
            // Print the Exception
            ex.printStackTrace();
			
		} 
		
		return book;

	}

	public void delete(long bookId) {
		Book book = new Book();
		book.setId(bookId);

		this.delete(book);
	}
	
	public void delete(Book book) {
		
		try {
//			transaction = manager.getTransaction();
//			
//			transaction.begin();

			if (! manager.contains(book)) {
				book = manager.merge(book);
			}
			
			manager.remove(book);
//			transaction.commit();
			
		} catch (Exception ex) {
            // If there are any exceptions, roll back the changes
//            if (transaction != null) {
//                transaction.rollback();
//            }
            // Print the Exception
            ex.printStackTrace();
			
		} 


	}
	
	public Book getOrCreate(String title) {
		
		Book found = null;
		
		try {
//			transaction = manager.getTransaction();
//			
//			transaction.begin();

			List<Book> books = (List<Book>) manager.createNamedQuery("findBook").setParameter("title", title).getResultList();
			
			if (books.size() > 1) {
				
				throw new Exception("Too many books with the passed parameters: " + books.size());
			} else if (books.size() == 1) {
				found = books.get(0);

			} else {
				found = new Book();
				found.setTitle(title);
				manager.persist(found);
				
			}
			
//			transaction.commit();
			
		} catch (Exception ex) {
            // If there are any exceptions, roll back the changes
//            if (transaction != null) {
//                transaction.rollback();
//            }
            // Print the Exception
            ex.printStackTrace();
			
		} 

		
		return found;
	}

	
	public List<Book> find(String title) {
		// NB below we use createNamedQuery, not createQuery! 
		Query findBooks = manager.createQuery("SELECT b FROM Book b WHERE title LIKE CONCAT('%',:title,'%')");
		findBooks = findBooks.setParameter("title", title);
		List<Book> books = (List<Book>) findBooks.getResultList();
		return books;
	}

}