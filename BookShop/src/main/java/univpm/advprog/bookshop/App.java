package univpm.advprog.bookshop;

import java.io.FileInputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.transaction.Transaction;

import org.apache.log4j.PropertyConfigurator;


public class App {
	
	public static void setupLogger() {
		Properties props = new Properties();
		try {
			props.load(new FileInputStream("/log4j.properties"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		PropertyConfigurator.configure(props);
	}
	
	public static void main(String[] args) {

		setupLogger();
		
		EntityManagerFactory emf = Persistence
					.createEntityManagerFactory("puBookStore");
		
		EntityManager em = emf.createEntityManager();
		EntityTransaction et = em.getTransaction();
		
		et.begin();
		
		AuthorManager aManager = new AuthorManager(em);
		BookManager bManager = new BookManager(em);
		
		aManager.setBookManager(bManager);
		bManager.setAuthorManager(aManager);
		Author author;
		
		try {
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
//			aManager.create("Arthur Conan", "Doyle", df.parse("1859-05-22"));
			author = aManager.getOrCreate("Arthur Conan", "Doyle");
			author.setBirthDate(df.parse("1859-05-22"));
			aManager.update(author);
			
			//em.flush();
			et.commit();
			
			et.begin();
			
			Book book = bManager.create("Uno studio in rosso",  10.50f, "Arthur Conan", "Doyle");
			assert em.contains(book);  // the instance is persistent NB since we drop using transactions, we can't test for getId() > 0
			
			// TODO compare result with and without flush
			//em.flush();
			et.commit();
			
			et.begin();
			
			List<Book> books = bManager.find("Uno studio");
			assert books.size() == 1;
			assert books.get(0).getId() == book.getId();
			assert books.get(0) == book;
			System.out.println("Author: " + author.getLastName() + " - # books: " + author.getBooks().size());

			
			bManager.update(book.getId(), "Alice nel paese delle meraviglie", 21.00f,  "Lewis", "Carroll");
			
			Book book2 = bManager.create("Il segno dei quattro", 12f, "Arthur Conan", "Doyle");
			
			
			et.commit();
			
			et.begin();
			//em.flush();
			
			System.out.println("Author: " + author.getLastName() + " - # books: " + author.getBooks().size());
			
			bManager.delete(book);
			bManager.delete(book2);
			et.commit();
			
		} catch (Exception ex) {
			et.rollback();
			ex.printStackTrace();
		}
		
	}

}
