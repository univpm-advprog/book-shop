package univpm.advprog.bookshop;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@NamedQueries({
		@NamedQuery(name = "readAuthor", query = "SELECT a FROM Author a WHERE id = :myID"),
		@NamedQuery(name = "findAuthorByName", query= "SELECT a FROM Author a WHERE firstName = :firstName AND lastName = :lastName")
}
)
@Entity
@Table(name = "author", uniqueConstraints=@UniqueConstraint(columnNames = { "firstName", "lastName" }))
public class Author {
	
	private long id;
	protected String firstName;
	protected String lastName;
	protected Date birthDate;
	public Set<Book> books = new HashSet<Book>();
	
	public Author() {
	}

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Column
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Column
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Column(nullable = true)
	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	
	@ManyToMany(mappedBy="authors", fetch = FetchType.LAZY)
	public Set<Book> getBooks() {
		return this.books;
	}
	
	public void setBooks(Set<Book> books) {
		this.books = books;
	}
	
	public void addBook(Book b) {
		this.books.add(b);
	}
}
